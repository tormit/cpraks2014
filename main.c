#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lib/sqlite3/sqlite3.h"
#include "lib/csv_parser/csv.h"
#include "lib/nasm_extra/tormi_nasm.h"

#define DB_NAME "data/test.db"
#define CSV_NAME "data/test.csv"

/**
 * Open new connection
 */
sqlite3* openConnection() {
	sqlite3 *db;
	int result;

	result = sqlite3_open(DB_NAME, &db);
	if (result == SQLITE_OK) {
		return db;
	}
	
	printf("Could not open connection to '%s'. Error: %d. Aborting...\n", DB_NAME, result);
	exit(0);
}

/**
 * Close open connection
 */
int closeConnection(sqlite3* db) {
	int result = sqlite3_close(db);
	if(result != SQLITE_OK) {
		fprintf(stderr, "Connection close error: %d\n", result);
	}

	return result;
}

/**
 * Create table in database
 */
void createTable() {
	char *errorMsg = 0;
	char *sql;
	int result;  // query result

	sqlite3 *db = openConnection();

	sql = "CREATE TABLE IF NOT EXISTS entry ("  \
         "id 			 INTEGER PRIMARY KEY NOT NULL," \
         "date           TEXT    NOT NULL," \
         "time           TEXT    NOT NULL," \
         "application    TEXT    NOT NULL," \
         "content        TEXT);";

	result = sqlite3_exec(db, sql, NULL, 0, &errorMsg);
   if( result == SQLITE_OK ){
   	  fprintf(stdout, "Table created successfully\n");
   } else {      
   	  fprintf(stderr, "SQL error: %s\n", errorMsg);
      sqlite3_free(errorMsg);
   }

   closeConnection(db);
}

/**
 * Insert row into database
 */
void insertEntry(const char *date, const char *time, const char *application, const char *content) {
	char *errorMsg = 0; // if error occurs during query execution, then message is stored here
   	int result; // query result
   	char *sql;
   	char *sqlCommand = "INSERT INTO entry(date,time,application,content) VALUES(\"%s\",\"%s\",\"%s\",\"%s\")";

   	sql = (char*) malloc(strlen(sqlCommand) + strlen(date) + strlen(time) + strlen(application) + strlen(content) + 1); // +1 - \0

	sqlite3* db = openConnection();

	sprintf(sql, sqlCommand, date, time, application, content);

	result = sqlite3_exec(db, sql, NULL, 0, &errorMsg);
	if( result == SQLITE_OK ) {
   	  fprintf(stdout, "%s inserted\n", date);
   } else {      
   	  fprintf(stderr, "SQL error: %s\n", errorMsg);
      sqlite3_free(errorMsg);
   }

	closeConnection(db);
}

/**
 * Find size of file.
 */
size_t getFileSize(FILE *f) {
	size_t pos0, size;

	pos0 = ftell(f);
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, pos0, SEEK_SET);

	return size;
}

/**
 * Read given CSV file by file and parse it for insertion into database.
 */
void parseCsv(const char *filename, const char *delimiter, const char *enclosure) {
	FILE *csvFilePointer;
	char *line;
	//char **parsedLine;
	size_t csvFileSize = 0;

	csvFilePointer = fopen(filename, "rb");
	if (!csvFilePointer) {
		fprintf(stderr, "Could not open CSV file %s.\n", CSV_NAME);
		exit(0);
	}

	csvFileSize = getFileSize(csvFilePointer);

	// as we dont know the size of each line, allocate whole file length to line
	line = (char*) malloc(csvFileSize + 1); // CSV size + \0

	printf("Printing lines...\n");
	int res; // for testing
	while (fgets(line, csvFileSize + 1, csvFilePointer) != NULL) {
		printf("%s\n", line);

		res = parse_csv_line(line, delimiter, enclosure);
		printf("%d\n", res);
	}

	printf("End.\n");
}


int main (int argc, char const *argv[]) {	
	createTable();

	// testing insertion 
	insertEntry("2012-11-12", "12:34:45", "SMS", "Test string");

	parseCsv(CSV_NAME, ",", "\"");
	printf("%s\n", "Done.");

	printf("\n\nTesting NASM extra functions.\n");	
	printf("=================================\n");	

	// tormi_strlen
	printf("tormi_strlen:\n");	
	printf("Length of \"%s\" is %d.\n", "tormi_strlen", tormi_strlen("tormi_strlen"));	

	// tormi_strchr
	printf("\ntormi_strchr:\n");
	char *testStr = "This is a sample string";
	char testSearchChar = 's';
	char *pch;
	pch = tormi_strchr(testStr, testSearchChar);
	while (pch - testStr + 1 < tormi_strlen(testStr))
	{
		printf ("Found \"%c\" in \"%s\" at %d.\n", testSearchChar, testStr, pch - testStr + 1);
	    pch = tormi_strchr(pch + 1, testSearchChar);
	}


	// tormi_strcmp
	printf("\ntormi_strcmp:\n");
	char *str1 = "Sample string";
  	char *str2 = "Sample 2";
  	int result;
  	
  	result = tormi_strcmp(str1, str2);
  	if (result == 0) {
		printf("\"%s\" is same as \"%s\"\n", str1, str2);
  	}
  	else {
  		printf("\"%s\" is different from \"%s\"\n", str1, str2);	
  	}

	printf("\n\nAll end.\n");	
}