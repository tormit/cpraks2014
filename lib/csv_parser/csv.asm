; --------------------------------------
; csv.asm
; @brief	CSV data parser
; @author	Tormi Talv
; @date		19.01.2015
; --------------------------------------

SECTION .data
testString db "Test arguments - line: %s; delimiter: %s; encapsule: %s;", 10, 0;


SECTION .text
extern printf
global parse_csv_line

parse_csv_line:
	; setup stack frame
	push	ebp
	mov		ebp, esp

	mov		eax, DWORD[ebp + 16]

	; push arguments
	push	DWORD[ebp + 16] ; line
	push	DWORD[ebp + 12] ; delimiter
	push	DWORD[ebp + 8]  ; enclosure
	push	testString
	call	printf

	; balance stack
	;add		esp, 16
	

	;.loop:
	;add		eax, 4

	;cmp		eax, 0 ; find \0 or the end of string
	;jne		.loop
	
	;mov		eax, 0

	; restore stack frame
	mov		esp, ebp
	pop		ebp
	ret
	




