; --------------------------------------
; tormi_strchr.asm
; @brief	Find first occurance of character
; @author	Tormi Talv
; @date		02.02.2015
; --------------------------------------
SECTION .text

global tormi_strchr
tormi_strchr:
	; setup stack and make reference to passed argument
	push	edi

	mov		edi, [esp + 8]	; string
	mov		eax, [esp + 12]	; character code
	
	; use similar method to strlen
	; repeat scan string instruction while EDI = AL (has reach searched character)
	cld						; clear direction flag
	repne	scasb	

	mov		eax, edi;
	dec     eax				; return address of previous byte
	
	; restore stack frame
	pop		edi
	ret