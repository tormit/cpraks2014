; --------------------------------------
; tormi_strlen.asm
; @brief	Find length of string
; @author	Tormi Talv
; @date		02.02.2015
; --------------------------------------
SECTION .text

global tormi_strlen
tormi_strlen:
	; setup stack and make reference to passed argument
	push	edi
	mov		edi, [esp + 8]	 

	; initialize ecx(to maximum potetial string lenght) 
	sub		ecx, ecx		; ecx = 0
	not		ecx				; reverse bits to get maximum 32-bit number

	sub		al, al			; al = 0, save \0 to 8-bit lower half of 16-bit AX register
	cld						; clear direction flag
	
	; repeat scan string instruction while EDI = AL (has reach \0 byte)
	repne	scasb			
	
	; negate counter to get counted bytes; scasb has decreased counter register with every scan
	neg		ecx				
	dec 	ecx				; -1 (\0 byte) to get real string length

	; restore stack frame
	pop		edi
	lea		eax, [ecx - 1] ; return address of length
	ret
	




