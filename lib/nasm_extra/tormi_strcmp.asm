; --------------------------------------
; tormi_strcmp.asm
; @brief	Compare two strings
; @author	Tormi Talv
; @date		02.02.2015
; --------------------------------------

SECTION .text

global tormi_strcmp

tormi_strcmp:
	; setup stack frame
        push    ebp
        mov     ebp, esp

        push    esi
        push    ecx
        push    ebx

        mov     esi, [ebp + 8]  ; string1
        mov     edi, [ebp + 12] ; string2
scanNextCharacter:
        mov     al, [esi]       ; save character from string1
        mov     bl, [edi]       ; save character from string2
        cmp     al, bl          ; compare them
        jne     isDifferent     ; if not equal
        cmp     al, 0           ; if equal and null?
        je      isSame          ; string are equal
        inc     esi             ; no. search on
        inc     edi
        jmp     scanNextCharacter
isDifferent:
        jg      isBigger
        mov     eax, -1         ; string1 < string2
        jmp     exit
isSame:
        mov     eax, 0          ; string1 == string2
        jmp     exit
isBigger:
        mov     eax, 1          ; string1 > string2
exit:
        ; restore stack frame
        pop     ebx
        pop     ecx
        pop     esi

        pop     ebp
        ret