#pragma once // include guard

int tormi_strlen(const char *string);
char* tormi_strchr(const char *string, const int character);
int tormi_strcmp(const char *string1, const char *string2);