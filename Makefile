OBJDIR  = obj/

# NASM CSV parser
LIB_CSV_DIR  = lib/csv_parser/
LIB_CSV_SRCS = $(wildcard $(LIB_CSV_DIR)*.asm)
LIB_CSV_OBJS = $(LIB_CSV_SRCS:$(LIB_CSV_DIR)%.asm=$(OBJDIR)%.o)
LIB_CSV_OUT  = lib/csv_parser/csv.a

# NASM extra
LIB_NASME_DIR  = lib/nasm_extra/
LIB_NASME_SRCS = $(wildcard $(LIB_NASME_DIR)*.asm)
LIB_NASME_OBJS = $(LIB_NASME_SRCS:$(LIB_NASME_DIR)%.asm=$(OBJDIR)%.o)
LIB_NASME_OUT  = lib/nasm_extra/nasme.a

# Sqlite3
LIB_SQLITE3_OUT  = lib/sqlite3/sqlite3.a

LIBS  = $(LIB_SQLITE3_OUT) $(LIB_CSV_OUT) $(LIB_NASME_OUT)

# Põhiprogramm
LOGDB_SRCS  = $(wildcard *.c)
LOGDB_OBJS  = $(LOGDB_SRCS:%.c=$(OBJDIR)%.o)
OUT_DIR = bin/
OUT   = $(OUT_DIR)logdb

# GCC parameetrid
FLAGS = -m32 -Wall -O3 -std=c99


all: make_csv_lib make_nasme_lib $(LIB_CSV_OBJS) $(LOGDB_OBJS) $(OUT_DIR)
	gcc $(FLAGS) -o $(OUT) $(LOGDB_OBJS) $(LIBS)
$(OBJDIR)%.o: %.c
	gcc $(FLAGS) -c $*.c -o $(OBJDIR)$*.o
	

make_csv_lib: $(OBJDIR) $(LIB_CSV_OBJS)
	ar rcs $(LIB_CSV_OUT) $(LIB_CSV_OBJS)
make_nasme_lib: $(OBJDIR) $(LIB_NASME_OBJS)
	ar rcs $(LIB_NASME_OUT) $(LIB_NASME_OBJS)

$(OBJDIR):
	mkdir $(OBJDIR)
$(OUT_DIR):
	mkdir $(OUT_DIR)
	

$(OBJDIR)%.o: $(LIB_CSV_DIR)%.asm
	nasm -O3 -i$(LIB_CSV_DIR) -f elf32 $(LIB_CSV_DIR)$*.asm -o $(OBJDIR)$*.o

$(OBJDIR)%.o: $(LIB_NASME_DIR)%.asm
	nasm -O3 -i$(LIB_NASME_DIR) -f elf32 $(LIB_NASME_DIR)$*.asm -o $(OBJDIR)$*.o

# Clean ei sõltu ühestki sisendfailist nimega "clean"
.PHONY: clean
clean:
	rm -r $(OBJDIR) $(LIBOUT) $(LIB_CSV_OUT) $(LIB_NASME_OUT) $(OUT_DIR)

clean_db:
	rm -r data/test.db